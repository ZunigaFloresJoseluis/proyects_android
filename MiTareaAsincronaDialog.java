 private class MiTareaAsincronaDialog extends AsyncTask<ArrayList<String>, Void, String> {
        
        public String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while(itr.hasNext()){

                String key= itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            return result.toString();
        }
        @Override
        protected String doInBackground(ArrayList<String>... passing) {
            try{

                JSONObject postDataParams = new JSONObject();
                ArrayList<String> passed = passing[0];
                postDataParams.put("username", passed.get(0));
                postDataParams.put("password", passed.get(1));
                Log.e("params",postDataParams.toString());
                URL host=new URL("http://10.0.0.11:8080/login");

                HttpURLConnection conn =(HttpURLConnection) host.openConnection();

                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept","application/json");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(
                            new InputStreamReader(
                                    conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    sb.toString();
                    return sb.toString();


                }
                else {

                    return  new String("false : "+responseCode);

                }


            }catch (Exception e){
                return e.toString();
            }


        }

        @Override
        protected void onProgressUpdate(Void... params) {

            Toast.makeText(Login.this, "Tarea Progreso",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected void onPostExecute(String result) {
            // Codigo a realizar despues de tener los datos


        }

        @Override
        protected void onCancelled() {
            Toast.makeText(Login.this, "Tarea cancelada!",
                    Toast.LENGTH_SHORT).show();
        }
    }